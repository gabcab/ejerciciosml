n = input("Digite un número: ")

if int(n) % 2 == 0:
    print(f"El número {n} es par")
else:
    print(f"El número {n} es impar")