n = input("Digite un número: ")

if n.isnumeric():
    if int(n) == 100:
        print(f"{n} es igual a 100")
    elif int(n) < 100:
        print(f"{n} es menor a 100")
    else:
        print(f"{n} es mayor a 100")
else:
    print("Debe escribir un #")